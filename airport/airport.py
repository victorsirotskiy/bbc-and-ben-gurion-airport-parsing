import selenium.webdriver as webdriver
import time
from properties import SELENIUM_PATH
import datetime
import json
import os


class Flight_Bord_Arrival:
    __slots__ = ['__fly_object', '__driver']

    def __init__(self, driver):

        self.__fly_object = {
            "fly_company": '',
            "fly_number": '',
            "terminal": "",
            "scheduled_time": "",
            "scheduled_date": "",
            "estimated_time": "",
            "status": "",
            "coming_from": "",
            "type_bord": "",
            "url": 'https://www.iaa.gov.il/airports/ben-gurion/flight-board'
        }
        self.__driver = driver

    def __transform_date(self, obj, type_col):
        obj = str(obj).replace('<time> <strong>', '').replace('</div></time>', "")
        time_ = obj.split('</strong><div>')[0]
        date_ = obj.split('</strong><div>')[1] + "/" + str(datetime.date.today().year)
        print(time_, date_)
        self.__fly_object[f"{type_col}_time"] = time_
        self.__fly_object[f"{type_col}_date"] = date_

    def pars(self):
        try:

            list_of_objects = []
            driver = self.__driver
            fly_objects = driver.find_elements_by_xpath('//table[@id="flight_board-arrivel_table"]//tr[@role]')
            print(len(fly_objects))

            for el in fly_objects:
                company = el.find_element_by_css_selector('div.td-airline')
                self.__fly_object['fly_company'] = company.get_attribute('innerHTML')
                flight = el.find_element_by_css_selector('div.td-flight')
                self.__fly_object['fly_number'] = flight.get_attribute('innerHTML')
                terminal = el.find_element_by_css_selector('div.td-terminal')
                self.__fly_object['terminal'] = terminal.get_attribute('innerHTML')
                scheduled_time = el.find_element_by_css_selector('div.td-scheduledTime')
                self.__transform_date(scheduled_time.get_attribute('innerHTML'), "scheduled")
                estimated_time = el.find_element_by_css_selector('div.td-updatedTime')
                self.__fly_object['estimated_time'] = estimated_time.text
                coming_from = el.find_element_by_css_selector('div.td-city')
                self.__fly_object['coming_from'] = coming_from.text
                status = el.find_element_by_css_selector('.flight_board--container .table tbody .dt-cell [data-status]')
                self.__fly_object['status'] = status.text
                self.__fly_object['type_bord'] = "arrival"
                print(self.__fly_object)
                list_of_objects.append(dict(self.__fly_object))

            return list_of_objects
        except Exception as e:
            print(f'Flight_Bord.__pars method \n {e}')


class Flight_Bord_Departure:
    __slots__ = ['__fly_object', '__driver']

    def __init__(self, driver):

        self.__fly_object = {
            "fly_company": '',
            "fly_number": '',
            "terminal": "",
            "scheduled_time": "",
            "scheduled_date": "",
            "estimated_time": "",
            "counter-area": "",
            "coming_to": "",
            "type_bord": "",
            "url": 'https://www.iaa.gov.il/airports/ben-gurion/flight-board'
        }
        self.__driver = driver

    def __transform_date(self, obj, type_col):
        obj = str(obj).replace('<time> <strong>', '').replace('</div></time>', "")
        time_ = obj.split('</strong><div>')[0]
        date_ = obj.split('</strong><div>')[1] + "/" + str(datetime.date.today().year)
        print(time_, date_)
        self.__fly_object[f"{type_col}_time"] = time_
        self.__fly_object[f"{type_col}_date"] = date_

    def pars(self):
        try:
            list_of_objects = []
            driver = self.__driver
            fly_objects = driver.find_elements_by_xpath(
                '//table[@id="flight_board-departures_table"]/tbody/tr[@role="row"]')
            print(len(fly_objects))

            for el in fly_objects:
                company = el.find_element_by_css_selector('div.td-airline')
                self.__fly_object['fly_company'] = company.get_attribute('innerHTML')
                flight = el.find_element_by_css_selector('div.td-flight')
                self.__fly_object['fly_number'] = flight.get_attribute('innerHTML')
                terminal = el.find_element_by_css_selector('div.td-terminal')
                self.__fly_object['terminal'] = terminal.get_attribute('innerHTML')
                scheduled_time = el.find_element_by_css_selector('div.td-scheduledTime')
                self.__transform_date(scheduled_time.get_attribute('innerHTML'), "scheduled")
                estimated_time = el.find_element_by_xpath('//div[@class="td-updatedTime"]/time')
                self.__fly_object['estimated_time'] = estimated_time.get_attribute('innerHTML')
                coming_from = el.find_element_by_css_selector('div.td-city')
                self.__fly_object['coming_to'] = coming_from.get_attribute('innerHTML')
                status = el.find_element_by_xpath('//div[@class="td-status"]/div[@data-status]')
                self.__fly_object['status'] = status.get_attribute('innerHTML')
                self.__fly_object['type_bord'] = "departure"
                print(self.__fly_object)
                list_of_objects.append(dict(self.__fly_object))

            return list_of_objects
        except Exception as e:
            print(f'Flight_Bord.__pars method \n {e}')


class Air_port:
    __slots__ = ['__driver', '__arrival_id', '__departure_id', '__URL']

    def __init__(self):

        self.__driver = webdriver.Chrome(SELENIUM_PATH)
        self.__URL = 'https://www.iaa.gov.il/airports/ben-gurion/flight-board/'
        self.__arrival_id = 'tab-arrivel_flights-label'
        self.__departure_id = 'tab--departures_flights-label'

    def __del__(self):
        self.__driver.close()

    def __request(self):
        try:

            driver = self.__driver
            try:
                driver.get(self.__URL)

                arrival = driver.find_element_by_id(self.__arrival_id)
                arrival.click()
            except Exception as e:
                raise ConnectionError(e)
            return driver
        except Exception as e:

            print(f"__request method error: {e}")

    @staticmethod
    def __is_last_page(driver):
        current_page = driver.find_element_by_css_selector('span#numOfResults')
        current_page = current_page.text
        last_page = driver.find_element_by_css_selector('span#totalItems')
        last_page = last_page.text
        if last_page == current_page:
            return True
        return False

    def flight_bord_pars(self):
        """
        Getting all flights boards from https://www.iaa.gov.il/airports/ben-gurion/flight-board/
        :return: list of flights
        """
        list_of_fly_boards = []
        driver = self.__request()

        time.sleep(2)
        next_page = driver.find_element_by_css_selector("button#next")
        while not self.__is_last_page(driver):
            next_page.click()
            time.sleep(1)

        bord = Flight_Bord_Arrival(driver)
        list_of_fly_boards.extend(bord.pars())

        departure = driver.find_element_by_id(self.__departure_id)

        departure.click()
        time.sleep(5)

        while not self.__is_last_page(driver):
            next_page.click()
            time.sleep(1)

        bord_d = Flight_Bord_Departure(driver)
        list_of_fly_boards.extend(bord_d.pars())
        return list_of_fly_boards

    @staticmethod
    def download_flight_bord_to_json(boards_list):
        """
        downloading all boards to 'boards' folder.
        format file  '%d_%m_%Y_%H_%M_%S.json'
        :param boards_list: list()
        :return: void
        """

        os.chdir(path)
        current_date = datetime.datetime.now()
        dt_string = current_date.strftime("%d_%m_%Y_%H_%M_%S")

        with open(f'{dt_string}.json', 'w', encoding='utf-8') as file:
            json.dump(boards_list, file, ensure_ascii=False)


if __name__ == '__main__':
    path = 'boards'
    air = Air_port()
    air.download_flight_bord_to_json(air.flight_bord_pars())
else:
    path = 'airport/boards'
