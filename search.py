import os
import glob
import json


class Search:
    def __init__(self, path):
        self.path = path
        os.chdir(self.path)

    @staticmethod
    def __search_in_articles(word=""):
        object_list = []
        index = 1
        for file in glob.glob('*.json'):
            with open(file) as f:
                js = json.load(f)
                for value in js.values():
                    if str(word).lower() in str(value).lower() and js not in object_list:
                        object_list.append({index: js, "file": file})
                        index += 1
                    break
        return object_list

    @staticmethod
    def __choose_article(index, list_of_aticles=[]):
        if not isinstance(index, int):
            raise ValueError(" index must be Number")
        if 1 > index > len(list_of_aticles):
            raise ValueError(f'must be in range of 1 : {len(index)}')
        for art in list_of_aticles:
            if index in art.keys():
                return art

    def search_article_in_archive(self, word):
        """
        search for an article by a phrase in an URL, TITLE or TEXT
        :param word: string
        :return: list of articles
        """
        if isinstance(word, int):
            word = str(word)
        if not isinstance(word, str):
            raise ValueError('Input must be String or Number')
        word = str(word.strip())
        print(self.__search_in_articles(word))
        ind = int(input("enter index of article that you want... \n For getting all articles of search enter 0"))
        if ind == 0:
            return self.__search_in_articles(word)
        return [self.__choose_article(ind, self.__search_in_articles(word))]

    def search_fly_boards(self, word):
        """
               search for an fly by a phrase in an URL, TITLE or TEXT
               :param word: string
               :return: list of articles
               """
        list_of_flies = []
        for file in glob.glob('*.json'):
            with open(file) as f:
                list_of_flies.append({file: self.__search_in_board(json.load(f), word)})
        return list_of_flies

    def __search_in_board(self, board, word):
        list_of_fly = []
        for fly in board:
            for value in fly.values():
                if str(word).lower() in str(value).lower() and fly not in list_of_fly:
                    list_of_fly.append(dict(fly))
        return list_of_fly

