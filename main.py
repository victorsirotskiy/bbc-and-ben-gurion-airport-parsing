"""
    MAIN FILE
"""


class BBC_Func:
    @staticmethod
    def pars_bbc():
        """
        Extracting articles from Home-page BBC.COM
        And download to folder(articles) all articles to .json files
        :return: Void
        """
        from bbc.bbc import BBC_Home
        bbc = BBC_Home()
        print(bbc.download_all_articles(bbc.get_all_links()))

    @staticmethod
    def search(word):
        """
        search for an article by a phrase in an URL, TITLE or TEXT
        :param word: string
        :return: list of articles
        """
        from search import Search

        search = Search("bbc/articles").search_article_in_archive(word)

        return search


class Airport_func:

    def __init__(self):
        from airport.airport import Air_port
        self.airport = Air_port()

    def get_board(self):
        """
        Getting all (arrival and departure)flights from https://www.iaa.gov.il/airports/ben-gurion/flight-board/
        :return: list of flights
        """
        return self.airport.flight_bord_pars()

    def download_board(self):
        """
        downloading all boards to 'boards' folder.
        format file  '%d_%m_%Y_%H_%M_%S.json'
        :return: void
        """
        self.airport.download_flight_bord_to_json(self.airport.flight_bord_pars())

    @staticmethod
    def search(word):
        """
        Search in archive of boards the the flight/flights by String
        (URL, DATE ex:17(day)/11(month)/2020(year), TIME, type of FlY (Arrival/Departure), FLY Number
        Coming from etc... )
        :param word: string
        :return: list flights
        """
        from search import Search
        search = Search('airport/boards').search_fly_boards(word)
        return search


class Air_Port_Thread:

    @staticmethod
    def run_thread():
        """
        Start downloading every minutes boards
        :return: void
        """
        from thread_for_airport import Runner_Every_1_Min
        thread = Runner_Every_1_Min()
        thread.run()


if __name__ == '__main__':
    # bbc = BBC_Func()
    # bbc.pars_bbc()
    air = Airport_func()
    air.get_board()
    # print(air.search("ly 339"))
    # t = Air_Port_Thread()
    # t.run_thread()


