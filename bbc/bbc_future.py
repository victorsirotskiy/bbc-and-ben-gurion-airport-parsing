import requests
from bs4 import BeautifulSoup as bs

HEADERS = {"content-type": "text/html",
           "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)"
                         " Chrome/86.0.4240.193 Safari/537.36",
           "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/"
                     "avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"

           }


class Future_Page:
    __slots__ = ['__html', '__text', '__article', '__url', '__data_component', '__title']

    def __init__(self, url):
        self.__url = url
        self.__html = requests.get(url, headers=HEADERS)
        self.__article = bs(self.__html.text, "html.parser")
        self.__text = ''
        self.__title = ""

    def get_text(self):
        list_of_text = []
        article = self.__article
        list_of_paragraphs = article.find_all('p', attrs={'class': None})
        for p in list_of_paragraphs:
            list_of_text.append(p.get_text())
            if len(p.findChildren()) > 0:
                for ch in p.findChildren():
                    if ch.name != "a":
                        try:
                            list_of_text.remove(p.get_text())
                        except:
                            pass
        for text in list_of_text:
            self.__text += text
        return {'article': self.__text}


if __name__ == '__main__':
    f = Future_Page('https://www.bbc.com/future/article/20201111-the-flying-car-is-here-vtols-jetpacks-and-air-taxis')
    print(f.get_text())
