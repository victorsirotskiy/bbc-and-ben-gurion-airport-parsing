import requests
from bs4 import BeautifulSoup as bs

HEADERS = {"content-type": "text/html",
           "user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 "
                         "Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) "
                         "Chrome/86.0.4240.183 Mobile Safari/537.36",
           "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/"
                     "avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"

           }


class Sport_Page:
    __slots__ = ['__html', '__text', '__article', '__url', '__title']

    def __init__(self, url):
        self.__url = url
        self.__html = requests.get(url, headers=HEADERS)
        self.__article = bs(self.__html.text, "html.parser").find_all("article")
        self.__text = ''
        self.__title = ""

    def get_text_and_title(self):
        art_list = self.__article
        if len(art_list) == 0:
            return {}
        for art in art_list:
            try:
                header = art.find('header')
                self.__title = header.get_text()
                self.__article = art
                break
            except:
                pass

        list_of_text = []

        body = self.__article.find(class_='story-body')
        list_of_allowed_tags = ["p", 'h2', 'h3']
        texts = body.find_all(list_of_allowed_tags, text=True)
        for text in texts:
            if text.get_text() not in list_of_text and text.name != 'ul':
                list_of_text.append(text.get_text())
                self.__text += text.get_text()

        return {'title': self.__title, 'article': self.__text}


if __name__ == '__main__':
    sp = Sport_Page("https://www.bbc.com/sport/football/54945758")
    print(sp.get_text_and_title())

