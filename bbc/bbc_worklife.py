import requests
from bs4 import BeautifulSoup as bs

HEADERS = {"content-type": "text/html",
           "user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 "
                         "Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) "
                         "Chrome/86.0.4240.183 Mobile Safari/537.36",
           "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/"
                     "avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"

           }


class Work_Life_Page:
    __slots__ = ['__html', '__text', '__article', '__url', ]

    def __init__(self, url):
        self.__url = url
        self.__html = requests.get(url, headers=HEADERS)
        self.__article = bs(self.__html.text, "html.parser").find("article")
        self.__text = ''

    def get_text(self):
        if len(self.__article) == 0:
            return {}
        paragraphs = self.__article.find_all('p')
        for p in paragraphs:
            self.__text += p.get_text()
        return {"article": self.__text}


if __name__ == '__main__':
    food = Work_Life_Page\
        ('https://www.bbc.com/worklife/article/20201109-coronavirus-how-cities-travel-and-family-life-will-change')
    food.get_text()
