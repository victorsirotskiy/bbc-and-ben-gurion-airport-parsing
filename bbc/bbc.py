import requests
import json
import os
from bs4 import BeautifulSoup as bs

from bbc.bbc_culture import Culture_Page
from bbc.bbc_food import Food_Page
from bbc.bbc_future import Future_Page
from bbc.bbc_news import News_Page
from bbc.bbc_sport import Sport_Page
from bbc.bbc_travel import Travel_Page
from bbc.bbc_worklife import Work_Life_Page

HEADERS = {"content-type": "text/html",
           "user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 "
                         "Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) "
                         "Chrome/86.0.4240.183 Mobile Safari/537.36",
           "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/"
                     "avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"

           }


class BBC_Home:
    __slots__ = ['html', 'article_links_list', '__url_home', 'home_headers', "classes", 'temp_link_list']

    def __init__(self):

        self.__url_home = "https://www.bbc.com"
        self.home_headers = HEADERS
        self.html = requests.get(self.__url_home, headers=self.home_headers)
        self.classes = ["top-list-item__link", "block-link__overlay-link"]
        self.article_links_list = []
        self.temp_link_list = []

    def __find_links(self, class_=""):
        link_list = []
        soup = bs(self.html.text, "html.parser")
        self.html.close()

        for link in soup.find_all('a', class_=f"{class_}", href=True):
            if str(link['href']).startswith("/"):
                if str(self.__url_home + link['href']) not in self.temp_link_list:
                    link_list.append({'link': str(self.__url_home + link['href']), "class": link['class'],
                                      "title": link.get_text().strip()})
                    self.temp_link_list.append(str(self.__url_home + link['href']))
            else:
                if link['href'] not in self.temp_link_list:
                    link_list.append({"link": link['href'], "class": link['class'], "title": link.get_text().strip()})
                    self.temp_link_list.append(link['href'])
        return link_list

    def get_all_links(self):
        for class_ in self.classes:
            self.article_links_list.extend(self.__find_links(class_))
            print(len(self.article_links_list))
        return self.article_links_list

    @staticmethod
    def download_all_articles(list_articles):
        """

        :param list_articles: list
        :return: void
        """
        path = 'bbc/articles'
        os.chdir(path)

        for article in list_articles:
            js_dict = {}
            if "/news/" in article['link']:
                if "/food/" in article['link']:
                    print("check time")
                print(article['link'])
                art_object = News_Page(article["link"])
                art_data = art_object.get_text_and_title()
                js_dict = dict(art_data)
                js_dict["link"] = article['link']

            elif "/sport/" in article['link'] and "/sport/live/" not in article['link']:

                print(article['link'])
                sport_obj = Sport_Page(article["link"])
                sport_data = sport_obj.get_text_and_title()
                js_dict = dict(sport_data)
                js_dict["link"] = article['link']

            elif "/future/" in article['link'] and "/future/live/" not in article['link']:
                print(article['link'])
                future_obj = Future_Page(article['link'])
                future_data = future_obj.get_text()
                js_dict = dict(future_data)
                js_dict['link'] = article['link']
                js_dict['title'] = article['title']
            elif "/food/" in article['link']:
                print(article['link'])
                food_obj = Food_Page(article['link'])
                food_data = food_obj.get_text()
                js_dict = dict(food_data)
                js_dict['link'] = article['link']
                js_dict['title'] = article['title']
            elif "/food/" in article['link']:
                print(article['link'])
                food_obj = Food_Page(article['link'])
                food_data = food_obj.get_text()
                js_dict = dict(food_data)
                js_dict['link'] = article['link']
                js_dict['title'] = article['title']
            elif "/worklife/" in article['link']:
                print(article['link'])
                work_obj = Work_Life_Page(article['link'])
                work_data = work_obj.get_text()
                js_dict = dict(work_data)
                js_dict['link'] = article['link']
                js_dict['title'] = article['title']
            elif "/travel/" in article['link']:
                print(article['link'])
                travel_obj = Travel_Page(article['link'])
                travel_data = travel_obj.get_text()
                js_dict = dict(travel_data)
                js_dict['link'] = article['link']
                js_dict['title'] = article['title']
            elif "/culture/" in article['link']:
                print(article['link'])
                cult_obj = Culture_Page(article['link'])
                cult_data = cult_obj.get_text()
                js_dict = dict(cult_data)
                js_dict['link'] = article['link']
                js_dict['title'] = article['title']
            else:
                continue
            with open(f"{js_dict['link'].split('/')[-1]}.json", 'w') as file:
                json.dump(js_dict, file)
