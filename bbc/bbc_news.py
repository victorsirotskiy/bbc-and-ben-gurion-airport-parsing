import requests
from bs4 import BeautifulSoup as bs

HEADERS = {"content-type": "text/html",
           "user-agent": "Mozilla/5.0 (Linux; Android 6.0; Nexus 5 "
                         "Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) "
                         "Chrome/86.0.4240.183 Mobile Safari/537.36",
           "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/"
                     "avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"

           }


class News_Page:
    __slots__ = ['__html', '__text', '__article', '__url', '__data_component', '__title']

    def __init__(self, url):
        self.__url = url
        self.__html = requests.get(url, headers=HEADERS)
        self.__article = bs(self.__html.text, "html.parser").find("article")
        self.__text = ''
        self.__data_component = ["text-block", "crosshead-block"]
        self.__title = ""

    @staticmethod
    def __check_data_component(element):
        if element.attrs['data-component'] == 'crosshead-block':
            return True
        return False

    def get_text_and_title(self):
        texts_list = self.__article
        if len(texts_list) == 0:
            return {}
        for element in texts_list:

            p = element.find_all(text=True)
            if element.name == "header":
                self.__title = element.find('h1').get_text()

                continue

            if 'data-component' in element.attrs:
                if element.attrs['data-component'] not in self.__data_component:
                    continue
            else:
                continue

            if len(p) > 1:
                for a in p:
                    if not str(a).startswith(".css") and str(a) != "None":
                        if self.__check_data_component(element):
                            self.__text += f"\n{a}\n"
                        else:
                            self.__text += a

            else:
                if not str(p[0]).startswith(".css") and str(p[0]) != "None":
                    if self.__check_data_component(element):
                        self.__text += f"\n{p[0]}\n"
                    else:
                        self.__text += p[0]

        if self.__text == "":
            soup = self.__article.find_all('p')
            self.__title = self.__article.find('h1').get_text()
            for p in soup:
                self.__text += str(p.get_text())

        return {'title': self.__title, 'article': self.__text}


