import threading
from airport.airport import Air_port
#
# def update_df():
#     hello()
#     threading.Timer(60*1*1, update_df).start()


class Runner_Every_1_Min:
    """
        this class starts a minute-by-minute download of the airport table update
    """
    def run(self):
        def run_Airport():
            print("start")
            threading.Timer(60, run_Airport).start()
            airport = Air_port()
            airport.download_flight_bord_to_json(airport.flight_bord_pars())
            print("end")
        run_Airport()


if __name__ == '__main__':
    r = Runner_Every_1_Min()
    r.run()
